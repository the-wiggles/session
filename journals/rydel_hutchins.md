# Rydel Hutchins Journal

## 2/20/2024  -  2/23/2024:
```
- Worked on Wire Frame
- API/Microservice Drawing
- DDD on Excalidraw
- Completed MVP form
- Worked on API backend names and design
```

## 2/26/2024 - 3/01/2024
# 2/26/2024:
```
- Worked more on wire frame.
```

# 2/27/2024:
```
- Adjusted wire frame and APIs to better suit projects needs.  Our team presented the project wireframe (excalidraw) to the instructors.
```

# 2/28/2024:
```
- More feedback from instructors on our wireframe was given today.
- As a team, we completed the docker-compose.yaml file, and ensured that the FastAPI, PGAdmin and the base pages work as expected.
```

# 2/29/2024:
```
- As a team, we built our tables for users, bands, and venues based on our work from the wireframe-apis we created.
- My "aha" moment came when we got a PostgreSQL extension in VSCode and used it to connect to our server and run SQL queries flawlessly.
```

# 3/01/2024:
```
- As a team we coded the POST and GET methods for users.py and began working on the authentication backend.
- My 'aha' moment was when we merged the request, it came back with "pipeline errors" from flake8 running on gitlab, we then (as a group, on Gabe's shared screen) installed black and flake8 and ran them in the cloned folder in his branch.  We rejected the original merge and then successfully merged with no pipeline errors the next time.
```

## 3/04/2024 - 3/08/2024
# 3/04/2024
```
- Assisted Gabe with Bands and Venues endpoints.
- Begin work on the messenger
```

# 3/05/2024
```
- Authorization errors with Messenger.  Need something that can tell me what user is actually logged in.
```

# 3/06/2024
```
- Performed toubleshooting how token is handled so the page can be refreshed, but to no avail.  Also tried to implement a way to pass user between pages on the front-end.
```

# 3/07/2024
```
- Assisted with implementing GET and POST methods for Venues and Bands. Lots of boilerplate code really.
```

# 3/08/2024
```
- WebSockets functionality working on backend!!
```

## 3/11/2024 - 3/15/2024
# 3/11/2024
```
- Redid some of the front-end to match with Tailwind that Gabe set up.  My implementations got lost in a deleted local branch because others ended up implementing them first.
```

# 3/12/2024
```
- Assisted Gabe with git merge issues.  Needed UserContext so we could push forward with Messenger WebSockets
```

# 3/13/2024
```
- Turns out WebSockets functionality was never working on the backend.  Tried just about everything on the frontend to make it happen.
```

# 3/14/2024
```
- Messenger backend and front end were merged, I put in try-catch statements so we can still use the site even without full functionality of the Messenger and so others could try and work on it.
```

# 3/15/2024
```
- Venues frontend, created the way to navigate through the venues.  Styling is finished for Venue individual profile page.
```

## 3/18/2024 - 3/22/2024
# 3/18/2024
```
- Venues frontend is started.  The Venue creation form is proving trickier than what was originally expected.
```

# 3/19/2024
```
- Tried to get WebSockets to work for most of the day, but it just will not authenticate, it gives a Forbidden; could be a token error but it is unknown.
```

# 3/20/2024
```
- More WebSockets issues.  Trying just about everything.  Given up on it for now, focused entirely on Venues.
- Aha moment when I reorganized a lot of the way I was submitting VenuesCreate.
- Messenger front end will be implemented using front-end polling.
```

# 3/21/2024
```
- Venues is done, Max completed messenger, we scrapped WebSockets; will make it a stretch goal.  Though I realized the authorization errors we were having had to do with the token, implemented a way to pass the token through the URL but it still failed.  Perhaps it's because I attempt to authenticate the token using HTTP still.
- Created boilerplate code to use for POSTing.
```

# 3/22/2024
```
- Finished unit test for Venues endpoints.
- Adjusted Messenger frontend styling.
```