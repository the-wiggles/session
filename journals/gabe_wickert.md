# Journal

## Week of 2/23/2024
Our group, The Wiggles, worked on the DDD on excalidraw, detailing the Wire Frames for our application. We have completed the MVP document and submitted, and ensured that the application is set to be developed by our team on GitLAB.

## Week of 3/1/2024
### 2/26/2024
Today, we worked our wireframes and enpoint designs as a team, which was reviewed by Paul.

### 2/27/2024
Today we continued to plan out our endpoints and provide additional clarity to the structure of how the application will be built, as well as a plan for how the tables in our database would be implemented.

### 2/28/2024
As a team, we completed the docker-compose.yaml file, and ensured that the FastAPI, PGAdmin and the base pages work as expected.

### 2/29/2024
Today as a team we built out our tables and merged them. Max took the initiative and issued assignments to the group.
My "aha" moment for today was discovering that if you do not clear out your Dockerfile.dev from Docker Desktop (which are cached), then it can throw an error that you have already "deleted" a file, which causes the server to not start.

### 3/1/2024
Today, we created a POST and GET method for the "Users" entity, and began working on the Authentication backend.

### 3/4/2024
Worked on essential methods for the Bands and Venues endpoints, including GET, POST, PUT, and DELETE.

### 3/5/2024
Continued working on our backend endpoints as a team.

### 3/6/2024
Implemented more user methods, which I collaborated with Scott to have them be authenticated and approved by the GitLab pipeline; had an issue with the error handling that we had established.

### 3/7/2024
Today, I added in the POST and GET methods for Venues and Bands, these will be updated with backend authentication protection later.

### 3/8/2024
Today I imported Tailwind css and began to make the forms have some basic css.

### 3/11/2024
Today I got Tailwind CSS library to be properly imported, thanks to the assistance of Will. The tailwind config file was not in the correct folder.

### 3/12/2024
Frontend Authentication was implemented into the Login and Logout form, thanks to the help of a long day of debugging with Rydel and the guidance of Will for getting it started.

### 3/13/2024
Implemented Redux Toolkit onto the Sign Up form, added in the required js files and npm install in order to get it running as well. Began to utilize this as directed by Ivan in order to make a dynamic profile page.

### 3/14/2024
After getting the RTK working for the Sign Up form, consulted with Rosheen who pointed out that the majority of our project would be more simply handled without using Redux. Worked on the React hooks on most of our pages.

### 3/15/2024
Began working on trying to figure out how to store the logged in User's information in order to dynamically render it, got some good resources and information from Ivan and Paul for tackling the problem.

### 3/18/2024
Completed the UserContext React class, so that it can be used to contain the logged in User's information.

### 3/19/2024
Spent a good chunk of the day trying to get the Websockets to work with Rydel now that we have the UserContext, unfortuneately, we have been running into several issues, so I will be using my time to try to address other issues with the project, like the dynamic elements.

### 3/20/2024
Updated the README, worked on the Edit Profile Page with Max Ruhr. I was able to get the update to work for every component other than the Genre and Instruments, which Max implemented.

### 3/21/2024
Today I polished up the Bands functionality, the Lineup page is now dynamic and generates based off of the infomration within the database, and the Band Profile page is dynamic based on who is selected on the Lineup page. In the second part of the day, I began writing the unit testing for the messaging element.

### 3/22/2024
-Application Due!-
