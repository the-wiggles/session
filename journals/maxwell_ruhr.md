# Production Journal

## Week 1:

### Friday 2/23
* Today we forked our project repo and added all members to the project
* We worked on the wireframe design
* We submitted the MVP
* We began working on the API design

### Monday 2/26
* Today we continued working on our API design
* We started learning how to do MR's through gitlab and doing all that fun new stuff

## Week 2:

### Tuesday 2/27
* We finished up our API design and added additional endpoints
* Paul came through our room and we walked him through our design, he provided great insight
    into how to handle tables for our messaging. It all still feels pretty damn foreign though.
* We added additional endpoints per his advice and researched more about front end polling
* It's all good fun. Spirits are high-ish.

### Wednesday 2/28
* We got Dockered hard- or at least Gabe did. He experienced issues on Docker that were related
    to the build part of our yaml, but we got it sorted.
* We also got our docker-compose file ready with our database stuff and set up pgAdmin as well.
* I realized the order of services in the docker-compose file can affect how things startup when you
    run docker build...

### Thursday 2/29
* Code has officially begun, woop woop.
* We mob coded the SQL tables and made a number of migrations to get our tables made.
* I had a bit of an a-ha moment outside of this project learning and understanding dfs and bfs
    in relation to binary trees. That stuff is pretty cool.

### Friday 3/1
* Under the weather today
* We got successful post responses and gets for users! success. Now time to start authentication.
* Gitting the hang of git and issues and merging.
* Aha moment was understanding queries are using lists and need commas. And
    that CI/CD stuff is running linting which is cool.

## Week 3:

### Monday 3/4
* Tough one today with troubleshooting authorization stuff with scott. We're likely going to have to pick it up tomorrow.
* I was able to get venue create is finished today.
* Aha moment today was realizing/being thankful that authorization is something a lot of other libraries handle. ha. It's
    pretty complex stuff.


### Tuesday 3/5
* More work on Venue put today, which works!
* Also authorization works which is awesome. Scott really championed that.
* Aha moment was realizing email is used for a lot of immutable authorization stuff, but we're using username
    so we'll see what issues that brings.


### Wednesday 3/6
* Today we finished authorization again.. it seems like new issues keep popping up with implementing it, but we think it's good now.
* I'm definitely getting experience with the PM side of things, creating issues and assigning people to them. It's a good way to track
    progress and definitely helps me work on my organizational skills which have historically haven't been my strongsuit.
* That's also my aha moment! That being organized is immportant.


### Thursday 3/7
* Another day in the trenches. I'm basically trying to make an about_me table that gets created and tied to a user when the user is created.
* This would allow the user to include their instruments, genres, and influences to have populate their profile.
* This is also hard. I've added a lot of code. It works kind of but I feel like there's a better way to do it.
* My aha moment is that this is tough.


### Friday 3/8
* Today is more work on the about me section, I feel like there's going to be a better way to include genre's and have them be filterable.
* I've made good progress on incorporating genres into the app. It's still an uphill battle that I'll hopefully have an epiphany on
    this weekend.
* My aha moment today was realizing I could use cmd + f to insert commas and quotations on 700+ lines of genres and not have to do it
    one row at a time :)


## Week 4:
### Monday 3/11
* Start of the week, I've been working more with the Genre list and figuring out how to implement that.
* Also realized that pydantic will pass through stuff to the api if left  blank??
* I set up some stuff to try to prevent updating fields with "string"


### Tuesday 3/12
* Successfully got genres to post to users today!
* Now I'm going to do the same with instruments, but this list will be much shorter


### Wednesday 3/13
* Continued working on adding instruments to users today
* Going to have to figure out how best to do this on the front end
* I'm also going to have to figure out how a filter function will work for this


### Thursday 3/14
* Completed the filter function on our Jam page! It's pretty cool.
* Basically, you can type any keyword between user info, instrument, genre, and it'll continually update rendered users on the page
* My aha moment was this being able to be done in one big filter function, feels good because filtered lists gave me a hard time on CarCar


### Friday 3/15
* Today was another cool one. Made each Jam user card a navlink to click on to get a dynamically rendered profile page for them.
* This is where the messaging will take place! Likely a pretty central piece of the application


## Week 5
### Monday 3/18
* Waiting for messaging to be good to go, so we can fully test the rendered detail pages
* We will need to get authorization working to hold userContext, since that was something we needed to set so we can track logged in user.
* Scared of this one.


### Tuesday 3/19
* Our loggedinuser works but if you refresh the page it does not... good thing this is a SPA.
* Today I added "add instrument" and "add genre" on the front end for logged in users. It's not pretty but it works!


### Wednesday 3/20
* We are fast  approaching our due date, nerves have set in.
* We still need Websockets to work and unit tests.
* The team is working on debugging our WebSocket connection, but no dice. I made a separate front end polling branch with those endpoints
    juuuuust in case.
* Aha moment today was "Rosheen was right about websockets"


### Thursday 3/21
* Websockets are a no go. I pivoted and added front end polling messaging this morning and got it working on the react page as well.
* Messages between users work and it auto polls for new ones too!
* Little bug is it'll clear messages after a while so it's not quite persisting.
* Also trying to get unit tests to work today. That was an absolute pain. What passes in docker exec, doesn't pass on the gitlab pipeline now.


### Friday 3/22
* Presentation today with instructors. I hope we pass and finish the program I'm exhausted haha.
* Still trying to get the test to pass.
