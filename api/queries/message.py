from typing import List
from datetime import datetime
from pydantic import BaseModel

messages = []


class Message(BaseModel):
    sender: str
    content: str
    timestamp: datetime


async def send_message(message: Message):
    messages.append(message)
    return {"message": "Message sent successfully"}


async def get_messages() -> List[Message]:
    return messages


async def poll_messages(timestamp: datetime) -> List[Message]:
    new_messages = [
        message for message in messages if message.timestamp > timestamp
    ]
    return new_messages
