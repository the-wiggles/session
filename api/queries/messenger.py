from pydantic import BaseModel
from queries.pool import pool
from datetime import datetime


class MessageIn(BaseModel):
    sender_user: str
    receiver_user: str
    content: str


class MessageOut(BaseModel):
    id: int
    msg_timestamp: datetime
    sender_user: str
    receiver_user: str
    content: str


class MessengerRepository:
    def get_messages(self, user_one, user_two):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT * FROM messenger
                        WHERE (sender_user = %s AND receiver_user = %s)
                        OR (sender_user = %s AND receiver_user = %s)
                        ORDER BY msg_timestamp
                        """,
                        [user_one, user_two, user_two, user_one],
                    )
                    records = result.fetchall()
                    if records is None:
                        return None
                    return [
                        MessageOut(
                            id=record[0],
                            msg_timestamp=record[1],
                            sender_user=record[2],
                            receiver_user=record[3],
                            content=record[4],
                        )
                        for record in records
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve messages"}

    def send_message(
        self,
        message: MessageIn,
    ):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO messenger (
                            msg_timestamp,
                            sender_user,
                            receiver_user,
                            content
                        )
                        VALUES
                            (CURRENT_TIMESTAMP, %s, %s, %s)
                        RETURNING id, msg_timestamp;
                        """,
                        [
                            message.sender_user,
                            message.receiver_user,
                            message.content,
                        ],
                    )
                    record = result.fetchone()
                    id, msg_timestamp = record[0], record[1]
                    old_data = message.dict()
                    return MessageOut(
                        id=id, msg_timestamp=msg_timestamp, **old_data
                    )
        except Exception as e:
            print(e)
            return {"message": "Failed to send message."}
