from pydantic import BaseModel

from fastapi.exceptions import HTTPException
from queries.pool import pool
from typing import Union, List, Optional


class Error(BaseModel):
    message: str
    hashed_password: str


class DuplicateUserError(ValueError):
    pass


class UserIn(BaseModel):
    username: str
    password: str
    first_name: str
    last_name: str
    email: str
    photo: str
    genres: List[str]
    instruments: List[str]


class UserOut(BaseModel):
    id: int
    username: str
    first_name: str
    last_name: str
    email: str
    photo: str
    genres: List[str]
    instruments: List[str]


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserRepository:
    def get_one_user(self, username: str) -> UserOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM users
                    WHERE username = %s
                    """,
                    [username],
                )
                result = db.fetchone()
                if result is None:
                    raise Exception("User not found")

                result = db.execute(
                    """
                    SELECT id
                        , hashed_password
                        , username
                        , first_name
                        , last_name
                        , email
                        , photo
                        , genres
                        , instruments
                    FROM users
                    WHERE username = %s
                    """,
                    [username],
                )
                record = result.fetchone()
                if record is None:
                    return None
                user_data = self.record_to_user_out(record).dict()
                return UserOutWithPassword(**user_data)

    def create(
        self, user: UserIn, hashed_password: str
    ) -> UserOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM users
                    WHERE username = %s
                    """,
                    [user.username],
                )
                result = db.fetchone()
                if result is not None:
                    raise Exception("Username already exists")

                db.execute(
                    """
                    SELECT * FROM users
                    WHERE email = %s
                    """,
                    [user.email],
                )
                result = db.fetchone()
                if result is not None:
                    raise Exception("Email already exists")

                result = db.execute(
                    """
                    INSERT INTO users (
                        username,
                        hashed_password,
                        first_name,
                        last_name,
                        email,
                        photo,
                        genres,
                        instruments
                    )
                    VALUES
                        (%s, %s, %s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        user.username,
                        hashed_password,
                        user.first_name,
                        user.last_name,
                        user.email,
                        user.photo,
                        user.genres,
                        user.instruments,
                    ],
                )
                id = result.fetchone()[0]
                old_data = user.dict()
                return UserOutWithPassword(
                    hashed_password=hashed_password, id=id, **old_data
                )

    def get_all(self):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            username,
                            first_name,
                            last_name,
                            email,
                            photo,
                            genres,
                            instruments
                        FROM users
                        ORDER BY id
                        """
                    )
                    return [
                        UserOut(
                            id=record[0],
                            username=record[1],
                            first_name=record[2],
                            last_name=record[3],
                            email=record[4],
                            photo=record[5],
                            genres=record[6],
                            instruments=record[7],
                        )
                        for record in db
                    ]
        except Exception:
            return {"Message": "Could not get all users"}

    def update_user(
        self,
        username: str,
        user: UserIn,
        hashed_password: Optional[str] = None,
    ) -> Union[UserOutWithPassword, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    result = db.fetchone()
                    if result is None:
                        raise HTTPException(
                            status_code=404, detail="User not found"
                        )

                    query = "UPDATE users SET "
                    parameters = []

                    if user.username != "string":
                        query += "username = %s, "
                        parameters.append(user.username)
                    if user.first_name != "string":
                        query += "first_name = %s, "
                        parameters.append(user.first_name)
                    if user.last_name != "string":
                        query += "last_name = %s, "
                        parameters.append(user.last_name)
                    if user.email != "string":
                        query += "email = %s, "
                        parameters.append(user.email)
                    if user.photo != "string":
                        query += "photo = %s, "
                        parameters.append(user.photo)
                    if user.genres:
                        query += "genres = %s, "
                        parameters.append("{" + ",".join(user.genres) + "}")
                    if user.instruments:
                        query += "instruments = %s, "
                        parameters.append(
                            "{" + ",".join(user.instruments) + "}"
                        )

                    query = query.rstrip(", ")

                    if hashed_password is not None:
                        query += ", hashed_password = %s"
                        parameters.append(hashed_password)

                    query += " WHERE username = %s RETURNING *;"
                    parameters.append(username)

                    db.execute(query, parameters)
                    record = db.fetchone()
                    if record is None:
                        return None
                    return self.user_in_to_out(record)
        except HTTPException as error:
            raise error

        except Exception as e:
            print(e)
            return {"Message": "Could not update user."}

    def delete(self, username: str) -> dict:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM users
                    WHERE username = %s
                    """,
                    [username],
                )
                result = db.fetchone()
                if result is None:
                    raise Exception("User not found")

                db.execute(
                    """
                    DELETE FROM users
                    WHERE username = %s

                    """,
                    [username],
                )
                return {"status": True, "username": username}

    def user_in_to_out(self, record: UserIn):
        user_data = self.record_to_user_out(record).dict()
        return UserOutWithPassword(**user_data)

    def record_to_user_out(self, record):
        return UserOutWithPassword(
            id=record[0],
            hashed_password=record[1],
            username=record[2],
            first_name=record[3],
            last_name=record[4],
            email=record[5],
            photo=record[6],
            genres=record[7],
            instruments=record[8],
        )
