steps = [
    [
        # UP SQL Statement
        """
        ALTER TABLE users
        ADD COLUMN instruments TEXT ARRAY NULL;
        """,
        # DOWN SQL Statement
        """
        ALTER TABLE users
        DROP COLUMN instruments;
        """,
    ]
]
