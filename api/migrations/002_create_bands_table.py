steps = [
    [
        # "Up" SQL Statement
        """
        CREATE TABLE bands (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(50) NOT NULL,
            name VARCHAR(50) NOT NULL,
            email VARCHAR(200) NOT NULL,
            photo VARCHAR NULL
            -- playlist TEXT NULL, (assumed to be a URL or object)
            -- members TEXT NULL (model/VO/FK),
            -- details TEXT NULL (model/VO/FK),
        );
        """,
        # "Down" SQL Statement
        """
        DROP TABLE bands;
        """,
    ]
]
