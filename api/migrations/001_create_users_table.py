steps = [
    [
        # "Up" SQL Statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            hashed_password VARCHAR(1000) NOT NULL,
            username VARCHAR(50) UNIQUE NOT NULL,
            first_name VARCHAR(50) NOT NULL,
            last_name VARCHAR(50) NOT NULL,
            email VARCHAR(200) UNIQUE NOT NULL,
            photo VARCHAR(1000) NULL,
            genres TEXT ARRAY NULL
            -- about_me TEXT NULL (model/VO/FK),
            -- music TEXT NULL (model/VO/FK),
        );
        """,
        # "Down" SQL Statement
        """
        DROP TABLE users;
        """,
    ]
]
