steps = [
    [
        # "Up" SQL Statement
        """
        CREATE TABLE messenger (
            id SERIAL PRIMARY KEY NOT NULL,
            msg_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            sender_id INTEGER REFERENCES users(id),
            receiver_id INTEGER REFERENCES users(id),
            content TEXT NOT NULL
        );
        """,
        # "Down" SQL Statement
        """
        DROP TABLE messenger;
        """,
    ]
]
