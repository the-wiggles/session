steps = [
    [
        # UP SQL Statement
        """
        ALTER TABLE venues
        ADD COLUMN photo VARCHAR(1000) NULL;
        """,
        # DOWN SQL Statement
        """
        ALTER TABLE venues
        DROP COLUMN photo;
        """,
    ]
]
