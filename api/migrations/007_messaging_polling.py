steps = [
    [
        """
            CREATE TABLE Messages (
            message_id SERIAL PRIMARY KEY,
            sender_username TEXT REFERENCES Users(username),
            recipient_username TEXT REFERENCES Users(username),
            content TEXT,
            timestamp TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
        );
            """,
        """
            DROP TABLE Messages
            """,
    ]
]
