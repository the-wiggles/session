from fastapi import APIRouter
from typing import List
from queries.genres import GENRE_LIST
from queries.instruments import INSTRUMENT_LIST

router = APIRouter()


@router.get("/genres", response_model=List[str])
async def get_genres():
    return GENRE_LIST


@router.get("/instruments", response_model=List[str])
async def get_instruments():
    return INSTRUMENT_LIST
