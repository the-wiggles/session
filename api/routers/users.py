from fastapi import (
    APIRouter,
    Depends,
    Response,
    HTTPException,
    status,
    Request,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel
from typing import Union, List
from queries.users import (
    Error,
    UserRepository,
    UserIn,
    UserOut,
    UserOutWithPassword,
)
from queries.genres import GENRE_LIST
from queries.instruments import INSTRUMENT_LIST


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    user: UserOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if user and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "user": user,
        }


@router.post("/api/users", response_model=AccountToken | HttpError)
async def create_user(
    info: UserIn,
    request: Request,
    response: Response,
    users: UserRepository = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        user = users.create(info, hashed_password)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e),
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, users)
    return AccountToken(user=user, **token.dict())


@router.get("/users", response_model=Union[List[UserOut], Error])
def get_all(
    repo: UserRepository = Depends(),
):
    return repo.get_all()


@router.get("/users/{username}", response_model=UserOutWithPassword)
def get_one_user(
    username: str,
    response: Response,
    repo: UserRepository = Depends(),
) -> UserOutWithPassword:
    try:
        user = repo.get_one_user(username)
        return user
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"{str(e)}",
        ) from e


@router.put(
    "/users/{username}", response_model=Union[UserOutWithPassword, Error]
)
def update_user(
    username: str,
    user: UserIn,
    repo: UserRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[Error, UserOutWithPassword]:
    try:
        hashed_password = authenticator.hash_password(user.password)
        return repo.update_user(username, user, hashed_password)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Could not update user",
        ) from e


@router.delete("/users/{username}", response_model=dict)
def delete_user(
    username: str,
    repo: UserRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> dict:
    try:
        deleted_user = repo.delete(username)
        return deleted_user
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"{str(e)}",
        ) from e


@router.put("/users/{username}/genres/", response_model=UserOutWithPassword)
async def add_genres_to_user(
    username: str, genres: List[str], repo: UserRepository = Depends()
) -> UserOutWithPassword:
    user = repo.get_one_user(username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    invalid_genres = [genre for genre in genres if genre not in GENRE_LIST]
    if invalid_genres:
        raise HTTPException(
            status_code=400,
            detail=f"Invalid genres: {', '.join(invalid_genres)}",
        )
    user.genres.extend(genres)
    updated_user = repo.update_user(username, user)
    return updated_user


@router.put(
    "/users/{username}/instruments/", response_model=UserOutWithPassword
)
async def add_instruments_to_user(
    username: str, instruments: List[str], repo: UserRepository = Depends()
) -> UserOutWithPassword:
    user = repo.get_one_user(username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    invalid_instruments = [
        instrument
        for instrument in instruments
        if instrument not in INSTRUMENT_LIST
    ]
    if invalid_instruments:
        raise HTTPException(
            status_code=400,
            detail=f"Invalid instruments: {', '.join(invalid_instruments)}",
        )
    user.instruments.extend(instruments)
    updated_user = repo.update_user(username, user)
    return updated_user
