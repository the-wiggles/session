from fastapi import APIRouter, Depends, HTTPException, Response, status
from typing import Union, List, Optional
from queries.venues import Error, VenueRepository, VenueIn, VenueOut
from authenticator import authenticator

router = APIRouter()


@router.post("/api/venues", response_model=Union[VenueOut, Error])
def create_venue(
    venue: VenueIn,
    response: Response,
    repo: VenueRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        return repo.create(venue)
    except Error as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        ) from e


@router.get("/venues/{venue_id}", response_model=Optional[VenueOut])
def get_one_venue(
    venue_id: int,
    repo: VenueRepository = Depends(),
) -> VenueOut:
    try:
        venue = repo.get_one(venue_id)
        return venue
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"{str(e)}",
        ) from e


@router.get("/venues", response_model=Union[List[VenueOut], Error])
def get_all(
    repo: VenueRepository = Depends(),
):
    try:
        return repo.get_all()
    except Error as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Internal Server Error: {str(e)}",
        ) from e


@router.delete("/venues/{venue_id}", response_model=bool)
def delete_venue(
    venue_id: int,
    response: Response,
    repo: VenueRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    try:
        is_deleted = repo.delete(venue_id)
        return is_deleted
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"{str(e)}",
        )


@router.put("/venues/{venue_id}", response_model=Union[VenueOut, Error])
def update_venue(
    venue_id: int,
    venue: VenueIn,
    repo: VenueRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[Error, VenueOut]:
    try:
        updated_venue = repo.update(venue_id, venue)
        return updated_venue
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        ) from e
