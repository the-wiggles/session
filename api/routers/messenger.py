from fastapi import (
    APIRouter,
    WebSocket,
    WebSocketDisconnect,
    Depends,
    HTTPException,
    status,
    Query,
)
from queries.messenger import MessengerRepository, MessageIn, MessageOut
from typing import List
from queries.users import UserRepository, UserOut
from authenticator import authenticator

router = APIRouter()


class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def send_personal_message(self, message: dict, websocket: WebSocket):
        await websocket.send_json(message)

    async def broadcast(self, message: dict):
        for connection in self.active_connections:
            await connection.send_json(message)


manager = ConnectionManager()
messenger_repo = MessengerRepository()
user_repo = UserRepository()


# WebSocket
# How to use endpoint on the front-end:
#   `${API_WS_HOST}/api/ws/${loggedInUser}/${receiverUsername}?token=${token}`
#   The token is validated on line 52 with the token at the end of the URL
@router.websocket("/api/ws/{sender_username}/{receiver_username}")
async def websocket_endpoint(
    websocket: WebSocket,
    sender_username: str,
    receiver_username: str,
    token: str = Query(...),
    current_user: UserOut = Depends(authenticator.get_current_user_websocket),
):
    sender_user = user_repo.get_one_user(sender_username)
    receiver_user = user_repo.get_one_user(receiver_username)

    if sender_user is None or receiver_user is None:
        print("error")
        await websocket.close(code=1008)
        return

    if current_user["username"] != sender_username:
        await websocket.close(code=1008)
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Unauthorized"
        )

    await manager.connect(websocket)
    try:
        while True:
            data = await websocket.receive_json()
            message = MessageIn(**data)

            saved_message = messenger_repo.send_message(message)

            receiver_websocket = next(
                (
                    conn
                    for conn in manager.active_connections
                    if conn.path_params["receiver_username"]
                    == receiver_username
                ),
                None,
            )
            if receiver_websocket:
                await manager.send_personal_message(
                    saved_message.dict(), receiver_websocket
                )

    except WebSocketDisconnect:
        manager.disconnect(websocket)


@router.get(
    "/api/messages/{sender_username}/{receiver_username}",
    response_model=List[MessageOut],
)
def get_messages(
    sender_username: str,
    receiver_username: str,
    current_user: dict = Depends(authenticator.get_current_account_data),
):
    print("routers sender_username: ", sender_username)
    print("routers receiver_username: ", receiver_username)
    print("routers current_user: ", current_user)
    sender_user = user_repo.get_one_user(sender_username)
    receiver_user = user_repo.get_one_user(receiver_username)
    print("routers sender_user: ", sender_user)
    print("routers receiver_user: ", receiver_user)
    if sender_user is None or receiver_user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )

    if (
        current_user["username"] != sender_username
        and current_user["username"] != receiver_username
        and not current_user.get("is_superuser", False)
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Unauthorized"
        )

    return messenger_repo.get_messages(
        sender_user.username, receiver_user.username
    )
