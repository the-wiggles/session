# import pytest
# from fastapi.testclient import TestClient
# from main import app
# from queries.message import Message, messages
# from queries.users import UserRepository, UserIn
# from datetime import datetime

# client = TestClient(app)


# @pytest.fixture(autouse=True)
# def setup():
#     yield
#     UserRepository().delete(username="sender_user")
#     UserRepository().delete(username="receiver_user")


# class TestMessaging:
#     def test_send_message(self):
#         # Arrange
#         sender_user = {
#             "username": "sender_user",
#             "password": "sender_password",
#             "first_name": "Sender",
#             "last_name": "User",
#             "email": "sender@example.com",
#             "photo": "",
#             "genres": [],
#             "instruments": []
#         }
#         receiver_user = {
#             "username": "receiver_user",
#             "password": "receiver_password",
#             "first_name": "Receiver",
#             "last_name": "User",
#             "email": "receiver@example.com",
#             "photo": "",
#             "genres": [],
#             "instruments": []
#         }

#         message_data = {
#             "sender": sender_user["username"],
#             "recipient": receiver_user["username"],
#             "content": "Hello, receiver!",
#             "timestamp": str(datetime.now().replace(microsecond=0, second=0))
#         }
#         UserRepository().create(
#             user=UserIn(**sender_user),
#             hashed_password=sender_user["password"],
#             )
#         UserRepository().create(
#             user=UserIn(**receiver_user),
#             hashed_password=receiver_user["password"],
#             )

#         # Act
#         response = client.post("/send_message/", json=message_data)
#         print("RESPONSE", response)

#         # Assert
#         assert response.status_code == 200
#         assert response.json() == {'message': 'Message sent successfully'}

#     def test_get_messages(self):
#         # Arrange
#         sender_user = {
#             "username": "sender_user",
#             "password": "sender_password",
#             "first_name": "Sender",
#             "last_name": "User",
#             "email": "sender@example.com",
#             "photo": "",
#             "genres": [],
#             "instruments": []
#         }
#         receiver_user = {
#             "username": "receiver_user",
#             "password": "receiver_password",
#             "first_name": "Receiver",
#             "last_name": "User",
#             "email": "receiver@example.com",
#             "photo": "",
#             "genres": [],
#             "instruments": []
#         }

#         # Create sender and receiver users
#         UserRepository().create(
#             user=UserIn(**sender_user),
#             hashed_password=sender_user["password"],
#             )
#         UserRepository().create(
#             user=UserIn(**receiver_user),
#             hashed_password=receiver_user["password"],
#             )

#         message1_data = {
#             "sender": sender_user["username"],
#             "content": "Hello, receiver!",
#             "timestamp": str(datetime.now().replace(second=0,
#                                                     microsecond=0).isoformat())
#         }

#         message1 = Message(**message1_data)

#         messages.extend([message1])

#         # Act
#         response = client.get(
#             f"/get_messages/{sender_user['username']}/"
#             f"{receiver_user['username']}"
#             )

#         assert response.status_code == 200

#         # Assert
#         response_json = response.json()
#         print("RESPONSE", len(response_json))
#         assert response_json[0]["sender"] == message1_data["sender"]
#         assert response_json[0]["content"] == message1_data["content"]
#         assert response_json[0]["timestamp"] == message1_data["timestamp"]
