from fastapi.testclient import TestClient
from main import app
from queries.bands import BandRepository
import pytest
import os

# from queries.users import UserRepository, UserIn, UserOutWithPassword
# import pytest

client = TestClient(app)


@pytest.fixture(autouse=True)
def set_env_vars():
    os.environ["SIGNING_KEY"] = "FFWoAg1IAf3+JLp7yD1FAv+6ijLsRTZ6S5xBwjTri8o="


class EmptyBandRepository:
    def get_all(self):
        return []


def test_get_all_bands():
    # ARRANGE
    app.dependency_overrides[BandRepository] = EmptyBandRepository
    # ACT
    response = client.get("/bands")
    app.dependency_overides = {}

    # ASSERT
    assert response.status_code == 200
    # assert response.json() == {"bands": []}
    assert response.json() == []


# @pytest.fixture(autouse=True)
# def setup_test_user():
#     # ARRANGE
#     app.dependency_overrides[UserRepository] = CreateTestUserRepository

#     # Create a test user
#     password = "testpassword"
#     hashed_password = bcrypt.hashpw(
#         password.encode("utf-8"), bcrypt.gensalt()
#     ).decode("utf-8")

#     user_data = UserIn(
#         username="testuser",
#         password=hashed_password,
#         first_name="Test",
#         last_name="User",
#         email="testuser@example.com",
#         photo="test photo",
#         genres=[],
#         instruments=[],
#     )

#     # ACT
#     response = client.post("/api/users", json=user_data.dict())
#     app.dependency_overrides = {}

#     # ASSERT
#     assert response.status_code == 200


# class CreateTestUserRepository:
#     def create(self, user: UserIn) -> UserOutWithPassword:
#         result = {
#             "id": 1010,
#             "hashed_password": "hashed_testpassword",
#         }
#         result.update(user.dict())
#         return UserOutWithPassword(**result)


# class CreateBandRepository:
#     def create_band(self, band):
#         result = {
#            "id": 1010,
#         }
#         result.update(band)
#         return result

# def test_create_band():
#     # ARRANGE
#     app.dependency_overrides[BandRepository] = CreateBandRepository

#     # Simulate a login
#     login_data = {
#         "username": "testuser",
#         "password": "testpassword",
#     }
#     login_response = client.post("/token", data=login_data)
#     token = login_response.json().get("access_token")

#     json = {
#         "username": "testuser",
#         "name": "Test User",
#         "email": "test email",
#         "photo": "test photo",
#     }
#     expected = {
#         "id": 1010,
#         "username": "testuser",
#         "name": "Test User",
#         "email": "test email",
#         "photo": "test photo",
#     }
#     # ACT
#     headers = {"Authorization": f"Bearer {token}"}
#     response = client.post("/bands", headers=headers, json=json)
#     # response = client.post("/bands")
#     app.dependency_overrides = {}
#     # ASSERT
#     assert response.status_code == 200
#     assert response.json() == expected


def test_init():
    assert 1 == 1
