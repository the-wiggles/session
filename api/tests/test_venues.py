from main import app
from fastapi.testclient import TestClient
from queries.venues import VenueRepository


client = TestClient(app)


class EmptyVenueRepository:
    def get_all(self):
        return []


def test_get_all_venues():
    app.dependency_overrides[VenueRepository] = EmptyVenueRepository
    response = client.get("/venues")
    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() == []
