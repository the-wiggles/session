export const GET_USER_DATA_REQUEST = 'GET_USER_DATA_REQUEST';
export const GET_USER_DATA_SUCCESS = 'GET_USER_DATA_SUCCESS';
export const GET_USER_DATA_FAILURE = 'GET_USER_DATA_FAILURE';

export const UPDATE_USER_REQUEST = 'UPDATE_USER_REQUEST';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
export const UPDATE_USER_FAILURE = 'UPDATE_USER_FAILURE';

// Action creators for getting user data
export const getUserDataRequest = () => ({
  type: GET_USER_DATA_REQUEST,
});

export const getUserDataSuccess = (userData) => ({
  type: GET_USER_DATA_SUCCESS,
  payload: userData,
});

export const getUserDataFailure = (error) => ({
  type: GET_USER_DATA_FAILURE,
  payload: error,
});

export const getUserData = (username) => async (dispatch) => {
  dispatch(getUserDataRequest());
  try {
    const response = await fetch(`http://localhost:8000/users/${username}`);
    if (!response.ok) {
      throw new Error('Failed to fetch user data');
    }
    const userData = await response.json();
    dispatch(getUserDataSuccess(userData));
  } catch (error) {
    dispatch(getUserDataFailure(error.message));
  }
};

// Action creators for updating user data
export const updateUserRequest = () => ({
  type: UPDATE_USER_REQUEST,
});

export const updateUserSuccess = (updatedUserData) => ({
  type: UPDATE_USER_SUCCESS,
  payload: updatedUserData,
});

export const updateUserFailure = (error) => ({
  type: UPDATE_USER_FAILURE,
  payload: error,
});

export const updateUser = (username, updatedUserData) => async (dispatch) => {
  dispatch(updateUserRequest());
  try {
    const response = await fetch(`http://localhost:8000/users/${username}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updatedUserData),
    });
    if (!response.ok) {
      throw new Error('Failed to update user data');
    }
    const userData = await response.json();
    dispatch(updateUserSuccess(userData));
  } catch (error) {
    dispatch(updateUserFailure(error.message));
  }
};
