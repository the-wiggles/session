import { useState, useEffect, useContext } from 'react'
import { UserContext } from './UserContext'
import { API_HOST } from '../constants'
import '../App.css'
import InstrumentList from '../InstrumentsList'

const AddInstrumentForm = ({ token }) => {
    const { loggedInUser } = useContext(UserContext)
    const [userData, setUserData] = useState(null)
    const [selectedInstrument, setSelectedInstrument] = useState('')
    const [error, setError] = useState(null)

    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const response = await fetch(
                    `${API_HOST}/users/${loggedInUser}`
                )
                if (!response.ok) {
                    throw new Error(
                        `Failed to fetch user data: ${response.status} ${response.statusText}`
                    )
                }
                const userData = await response.json()
                setUserData(userData)
            } catch (error) {
                console.error('Error fetching user data:', error)
                setError('Failed to fetch user data. Please try again.')
            }
        }

        if (loggedInUser) {
            fetchUserData()
        }
    }, [loggedInUser])

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const response = await fetch(
                `${API_HOST}/users/${loggedInUser}/instruments`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify([selectedInstrument]),
                }
            )

            if (!response.ok) {
                throw new Error(
                    `Failed to add instrument: ${response.status} ${response.statusText}`
                )
            }
            console.log('Instrument added successfully!')
        } catch (error) {
            console.error('Error adding instrument:', error)
            setError('Failed to add instrument. Please try again.')
        }
    }

    if (!userData) {
        return <div>Loading...</div>
    }

    return (
  <div className="mx-auto max-w-md p-4 py-20 bg-white rounded">
    <h1 className="text-3xl font-bold mb-4">Add Instrument</h1>
    {error && <p className="text-red-500">{error}</p>}
    <form onSubmit={handleSubmit} className="space-y-4">
      <div>
        <label htmlFor="instrument" className="block text-gray-600">Select Instrument:</label>
        <select
          id="instrument"
          value={selectedInstrument}
          onChange={(e) => {
            setSelectedInstrument(e.target.value);
            console.log(e.target.value);
          }}
          className="w-full p-2 border rounded mt-1"
        >
          <option value="">-- Select an instrument --</option>
          {InstrumentList.map((instrument) => (
            <option key={instrument} value={instrument}>
              {instrument}
            </option>
          ))}
        </select>
      </div>
      <button
        type="submit"
        className="w-full bg-lime-600 text-white py-2 rounded hover:bg-green-600 transition duration-300"
      >
        Add Instrument
      </button>
    </form>
  </div>
);
}

export default AddInstrumentForm
