const Footer = () => {
  return (
    <footer className="bg-slate-800 text-white py-4 flex-shrink-0">
      <div className="container mx-auto text-center">
        <p>&copy; 2024 Session. All rights reserved.</p>
      </div>
    </footer>
  );
};

export default Footer;
